﻿using FileStorage.DAL.EF;
using FileStorage.DAL.Entities;
using FileStorage.DAL.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace FileStorage.Tests.DataTests
{
    [TestClass]
    public class FileRepositoryTests
    {
        [TestMethod]
        public void FileRepository_GetAll_ReturnsAllValues()
        {
            var data = new List<File>
            {
                new File { Name = "1" },
                new File { Name = "2" },
                new File { Name = "3" },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<File>>();
            mockSet.As<IQueryable<File>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<File>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<File>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<File>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<FileStorageContext>();
            mockContext.Setup(c => c.Files).Returns(mockSet.Object);

            Repository<File> repository = new Repository<File>(mockContext.Object);
            var actual = repository.GetAll().Result.ToList();

            Assert.AreEqual(3, actual.Count);
        }
    }
}
